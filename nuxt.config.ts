import { defineNuxtConfig } from 'nuxt'

// https://v3.nuxtjs.org/api/configuration/nuxt.config

defineNuxtConfig({
    ssr: true
  })
export default defineNuxtConfig({
    modules: ['@nuxt/content'],
    head:{
        link : [
            { rel : 'stylesheet' , href : "https://font.sec.miui.com/font/css?family=MiSans:300,450,500,650:Chinese_Simplify,Latin&display=swap"}
        ]
    },
    css: ['@/assets/css/mdui.css','@/assets/css/miuiroms.css'],
})
